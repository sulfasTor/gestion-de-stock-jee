package gestiondestock.beans;

public class Provider {

	private String code;
	private String name;
	private String address;
	private String postal_code;
	private String telephone;
	
	
	public Provider(String code) {
		this.code = code;
	}

	public Provider(String code, String name, String address, String postal_code, String telephone) {

		this.code = code;
		this.name = name;
		this.address = address;
		this.postal_code = postal_code;
		this.telephone = telephone;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getPostal_code() {
		return postal_code;
	}


	public void setPostal_code(String postal_code) {
		this.postal_code = postal_code;
	}


	public String getTelephone() {
		return telephone;
	}


	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

}
