package gestiondestock.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class PostgresqlConnUtils {

	public static Connection getPostgresqlDBConnection(String hostName, String dbName,
			String userName, String password) throws SQLException,
	ClassNotFoundException {

		Class.forName("org.postgresql.Driver");
		String connectionURL = "jdbc:postgresql://" + hostName + ":5432/" + dbName;

		Connection conn = DriverManager.getConnection(connectionURL, userName,
				password);
		return conn;
	}
}