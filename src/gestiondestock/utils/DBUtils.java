package gestiondestock.utils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import gestiondestock.beans.Item;
import gestiondestock.beans.Provider;
import gestiondestock.beans.Order;

public class DBUtils {
	
	private static Connection conn = null;
	
	public DBUtils() {
		try {
			conn = ConnectionUtils.getConnection();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/* Items queries */

	public Item findItem(String code) throws SQLException {
		StringBuilder sql = new StringBuilder();
		sql.append("Select a.code, a.name, a.price, a.provider, a.stock_range, \n");
		sql.append("a.stock_quantity, a.sold_by_unit from items a where a.code=?;");

		PreparedStatement pstm = conn.prepareStatement(sql.toString());
		pstm.setString(1, code);

		ResultSet rs = pstm.executeQuery();

		while (rs.next())
			return new Item(code, rs.getString("name"), rs.getFloat("price"), new Provider(rs.getString("provider")),
					rs.getInt("stock_range"), rs.getInt("stock_quantity"), rs.getBoolean("sold_by_unit"));
		return null;
	}

	public void updateItem(Item product) throws SQLException {
		StringBuilder sql = new StringBuilder();

		sql.append("Update items set ");
		sql.append("name=?, price=?, provider=?, stock_range=?, stock_quantity=?, sold_by_unit=?");
		sql.append("where code=?;");

		PreparedStatement pstm = conn.prepareStatement(sql.toString());

		pstm.setString(1, product.getName());
		pstm.setFloat(2, product.getPrice());
		pstm.setString(3, product.getProvider().getCode());
		pstm.setInt(4, product.getStock_range());
		pstm.setInt(5, product.getStock_quantity());
		pstm.setBoolean(6, product.isSold_by_unit());

		pstm.executeUpdate();
	}
	
	public void updateQuantity(String code, int quantity) throws SQLException {
		StringBuilder sql = new StringBuilder();

		sql.append("Update items set ");
		sql.append("stock_quantity=?\n");
		sql.append("where code=?;");

		PreparedStatement pstm = conn.prepareStatement(sql.toString());
		pstm.setInt(1, quantity);
		pstm.setString(2, code);
		
		pstm.executeUpdate();
	}

	public void insertItem(Item product) throws SQLException {
		StringBuilder sql = new StringBuilder();

		sql.append("Insert into items");
		sql.append("(code, name, price, provider, stock_range, stock_quantity, sold_by_unit)");
		sql.append("values (?,?,?,?,?,?) ;");

		PreparedStatement pstm = conn.prepareStatement(sql.toString());

		pstm.setString(1, product.getName());
		pstm.setFloat(2, product.getPrice());
		pstm.setString(3, product.getProvider().getCode());
		pstm.setInt(4, product.getStock_range());
		pstm.setInt(5, product.getStock_quantity());
		pstm.setBoolean(6, product.isSold_by_unit());

		pstm.executeUpdate();
	}

	public void deleteItem(String code) throws SQLException {
		StringBuilder sql = new StringBuilder();

		sql.append("delete from items where code=? ;");

		PreparedStatement pstm = conn.prepareStatement(sql.toString());

		pstm.setString(1, code);
		pstm.executeUpdate();
	}

	/* Provider queries */
	
	public Provider findProvider(String code) throws SQLException {
		String sql = "Select a.code, a.name, a.address, a.postal_code, a.telephone from providers a where a.code";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, code);

		ResultSet rs = pstm.executeQuery();

		while (rs.next())
			return new Provider(code, rs.getString("name"), rs.getString("address"),
					rs.getString("postal_code"), rs.getString("telephone"));
		return null;
	}

	public void updateProvider(Provider provider) throws SQLException {
		StringBuilder sql = new StringBuilder();

		sql.append("Update providers set \n");
		sql.append("name=? , address=?, postal_code=?, telephone=?\n");
		sql.append("where code=?;");

		PreparedStatement pstm = conn.prepareStatement(sql.toString());

		pstm.setString(1, provider.getName());
		pstm.setString(2, provider.getAddress());
		pstm.setString(3, provider.getPostal_code());
		pstm.setString(4, provider.getTelephone());
		pstm.setString(5, provider.getCode());
		
		pstm.executeUpdate();
	}

	public void insertProvider(Provider provider) throws SQLException {
		StringBuilder sql = new StringBuilder();

		sql.append("Insert into providers\n");
		sql.append("(name, address, postal_code, telephone \n");
		sql.append("values (?,?,?,?) ;");

		PreparedStatement pstm = conn.prepareStatement(sql.toString());

		pstm.setString(1, provider.getName());
		pstm.setString(2, provider.getAddress());
		pstm.setString(3, provider.getPostal_code());
		pstm.setString(4, provider.getTelephone());

		pstm.executeUpdate();
	}

	public void deleteProvider(String code) throws SQLException {
		StringBuilder sql = new StringBuilder();

		sql.append("delete from providers where code=? ;");

		PreparedStatement pstm = conn.prepareStatement(sql.toString());

		pstm.setString(1, code);
		pstm.executeUpdate();
	}
	
	/* Orders queries */
	
	public Order findOrder(String code) throws SQLException {
		StringBuilder sql = new StringBuilder();
		sql.append("select a.code, a.ordered_on, b.itemCode, b.itemName, b.price from orders a\n");
		sql.append("inner join order_items b on b.orderCode	=a.code\n");
		sql.append("where a.code=? ;");

		PreparedStatement pstm = conn.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, 
                ResultSet.CONCUR_UPDATABLE);
		pstm.setString(1, code);

		ResultSet rs = pstm.executeQuery();

		List<Item> items = new ArrayList<Item>();
		while (rs.next())
			items.add(new Item(rs.getString("code"), rs.getString("itemName"), rs.getFloat("price")));
		
		if (items.size() > 0) {
			rs.first();
			return new Order(code, rs.getDate("ordered_on"), items);
		}
		return null;
	}
	
	public void updateOrder(String code, boolean closed) throws SQLException {
		String sql = "update orders set closed=? where code=?;";
		
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(2, code);
		pstm.setBoolean(1, closed);
		pstm.executeUpdate();
	}
	
	public void insertOrder(Order order) throws SQLException {
		StringBuilder sql = new StringBuilder();

		sql.append("Insert into orders\n");
		sql.append("(code, ordered_on)\n");
		sql.append("values (?,?) ;");
		
		PreparedStatement pstm = conn.prepareStatement(sql.toString());
		pstm.setString(1, order.getCode());
		pstm.setDate(2, order.getDate());
		
		pstm.executeUpdate();
		
		for (Item detail : order.getItems()) {
			StringBuilder sql1 = new StringBuilder();
			
			sql1.append("insert into order_items(orderCode, itemCode, itemName, price)\n");
			sql1.append("values(?,?,?,?);");
			PreparedStatement pstm1 = conn.prepareStatement(sql1.toString());
			pstm1.setString(1, order.getCode());
			pstm1.setString(2, detail.getCode());
			pstm1.setString(3, detail.getName());
			pstm1.setFloat(4, detail.getPrice());
			
			pstm1.executeUpdate();
		}
	}
	
	public void deleteOrder(String code) throws SQLException {
		StringBuilder sql = new StringBuilder();

		sql.append("delete from orders where code=? ;");
		sql.append("delete from order_items where orderCode=? ;");

		PreparedStatement pstm = conn.prepareStatement(sql.toString());

		pstm.setString(1, code);
		pstm.setString(2, code);
		pstm.executeUpdate();
	}
	
	public List<String> listOpenOrders(Date date, boolean status) throws SQLException {
		List<String> codes = new ArrayList<String>();
		
		String sql = "select a.code from orders a where a.ordered_on=? and a.closed=?;";
		
		PreparedStatement pstm = conn.prepareStatement(sql.toString());
		pstm.setDate(1, date);
		pstm.setBoolean(2, status);
		ResultSet rs = pstm.executeQuery();
		
		while(rs.next()) {
			codes.add(rs.getString("code"));
		}
		
		return codes;
	}
	
	/* Misc */
	
	public int getLastOrderIndex() throws SQLException {
		StringBuilder sql = new StringBuilder();
		int idx = 0, rowCount = 0;

		sql.append("select a.orderID id from orders a \n");
		sql.append("order by a.orderID DESC LIMIT 1;");
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql.toString());

		while (rs.next()) {
			idx = rs.getInt("id");
			rowCount++;
		}
		
		if (rowCount == 0)
			return 0;

		return idx;	
	}

}