package gestiondestock.servlet;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import gestiondestock.beans.Item;
import gestiondestock.beans.Order;
import gestiondestock.utils.StockManagement;

/**
 * Servlet implementation class GestionDeStok
 */
@WebServlet(description = "Servlet Gestion De Stock", urlPatterns = { "/" })
public class GestionDeStock extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static Order order;
	private static boolean order_finished = false;

	private static String MENU = "/WEB-INF/views/index.jsp";
	private static String ORDER_DETAIL = "/WEB-INF/views/orderDetail.jsp";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GestionDeStock() {
		super();
	}

	/**
	 * @see HttpServlet#init(ServletConfig config)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		String orderCode = null;

		try {
			orderCode = StockManagement.getNewOrderCode();
		} catch (SQLException e) {
			e.printStackTrace();
		} 	
		order = new Order(orderCode, new Date(Calendar.getInstance().getTimeInMillis()));
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {

		request.setAttribute("order", order);

		RequestDispatcher view = request.getRequestDispatcher(MENU);
		view.forward(request, response);
	}	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String forward;	

		if (request.getParameter("scanButton") != null) {
			forward = MENU;
			Item item_found = null;
			String code = request.getParameter("scanCode");
			try {
				item_found = StockManagement.querier.findItem(code);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			if (item_found != null) {
				order.addItem(item_found);
			} else {
				request.setAttribute("message", "Item not found.");
			}
			System.out.println("Item added.");
		} else if (request.getParameter("orderButton") != null) {
			System.out.println("Order set");
			if (order != null && order.getItems() != null && order.getItems().size() > 0) {
				forward = ORDER_DETAIL;
				try {
					StockManagement.finalizeOrder(order);
					order_finished = true;
				} catch (SQLException e) {
					e.printStackTrace();
					forward = MENU;
					request.setAttribute("message", "Error while processing the order.");
				}		
			} else {
				forward = MENU;
				request.setAttribute("message", "Order is empty.");
			}
		} else {
			System.out.println("Nothing happened.");
			return;
		}

		request.setAttribute("order", order);
		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response);	

		if (order_finished) {
			order_finished = false;
			order = null;
			String orderCode = null;

			try {
				orderCode = StockManagement.getNewOrderCode();
			} catch (SQLException e) {
				e.printStackTrace();
			} 	
			order = new Order(orderCode, new Date(Calendar.getInstance().getTimeInMillis()));
		}
	}

}
