package gestiondestock.servlet;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import gestiondestock.beans.Order;
import gestiondestock.utils.StockManagement;

/**
 * Servlet implementation class RegisterClosing
 */
//@WebServlet(description = "Servlet Gestion De Stock", urlPatterns = { "/RegisterClosing" })
public class RegisterClosing extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private static String CLOSING = "/WEB-INF/views/RegisterClosing.jsp";
	private static List<Order> orders = null;
	private static Double total = 0.0;
	private static Date today;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterClosing() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (orders == null || orders.size() == 0) {		
			try {
				java.util.Date currentDate = Calendar.getInstance().getTime();		
				today = new Date(currentDate.getTime());
				orders = StockManagement.getAllOrders(today);
				for (Order order : orders)
					total += order.getTotal();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		request.setAttribute("orders", orders);
		request.setAttribute("total", total);	
		RequestDispatcher view = request.getRequestDispatcher(CLOSING);
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (orders == null && orders.size() == 0)
			return;
		try {
			StockManagement.validateRegisterClosing(orders);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		orders = null;
		total = 0.0;
		doGet(request, response);
	}

}
