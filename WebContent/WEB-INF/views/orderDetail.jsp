<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Detail commande</title>

<style type="text/css">
<%@include file ="/resources/css/bootstrap.min.css"%>
</style>

</head>
<body>
	<h3 class="h3" >Détail de la commande: ${order.code} du ${order.date}</h3>
	<div class="jumbotron">
		<table class="table">
			<tr>
				<th>Produit</th>
				<th>Prix</th>
			</tr>
			<c:forEach var="item" items="${order.items}">
				<tr>
					<td><c:out value="${item.name}" /></td>
					<td><c:out value="${item.price}" /></td>
				</tr>
			</c:forEach>
			<tr>
				<th>Total</th>
				<td><fmt:formatNumber type="number" maxFractionDigits="2"
						value="${order.total}" /></td>
			</tr>
		</table>
	</div>
	<br>
	<a href="GestionDeStock">Nouvelle commande</a>
</body>
</html>