package gestiondestock.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MariaDBConnUtils {


	public static Connection getMariaDBConnection(String hostName, String dbName,
			String userName, String password) throws SQLException,
	ClassNotFoundException {

		Class.forName("org.mariadb.jdbc.Driver");
		String connectionURL = "jdbc:mariadb://" + hostName + ":3306/" + dbName;
		
		Connection conn = DriverManager.getConnection(connectionURL, userName,
				password);
		return conn;
	}
}