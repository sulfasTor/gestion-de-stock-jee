<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.Locale"%>
<%@ page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Etablissement du solde</title>
<%
	SimpleDateFormat sdf = new SimpleDateFormat("EEEEE, dd MMMM yyyy", Locale.FRANCE);
	String date = sdf.format(new Date());
%>
<style type="text/css">
<%@include file ="/resources/css/bootstrap.min.css"%>
</style>

</head>
<body>
	<h1 class="h1">
		Etablissement du solde du
		<%=date%>.
	</h1>
	<a href="GestionDeStock">Retour commande</a>
	<div class="jumbotron">
		<h3>Détails</h3>
		<table id="items" class="table">
			<tr>
				<th>No. Commande</th>
				<th>Produit</th>
				<th>Prix</th>
			</tr>
			<c:forEach var="order" items="${orders}">
				<c:forEach var="item" items="${order.items}">
					<tr>
						<td><c:out value="${order.code}" /></td>
						<td><c:out value="${item.name}" /></td>
						<td><c:out value="${item.price}" /></td>
					</tr>
				</c:forEach>
			</c:forEach>
		</table>
		<h3>Totaux</h3>
		<table id="totals" class="table">
			<tr>
				<th>No. Commande</th>
				<th>No. Articles</th>
				<th>Total</th>
			</tr>
			<c:forEach var="order" items="${orders}">
				<tr>
					<td><c:out value="${order.code}" /></td>
					<td><c:out value="${order.items.size()}" /></td>
					<td><fmt:formatNumber type="number"
							maxFractionDigits="2" value="${order.total}" /></td>
				</tr>
			</c:forEach>
			<tr class="total">
				<th>Total</th>
				<td></td>
				<td><fmt:formatNumber type="number"
						maxFractionDigits="2" value="${total}" /></td>
			</tr>
		</table>
		<hr>
		<form name="validate" action="RegisterClosing" method="post">
			<button class="btn btn-success" type="submit" name="validateButton">Valider</button>
		</form>
	</div>
</body>
</html>