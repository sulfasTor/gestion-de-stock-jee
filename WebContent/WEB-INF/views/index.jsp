<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.Locale"%>
<%@ page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Gestion de Stock</title>
<style type="text/css">
<%@include file ="/resources/css/bootstrap.min.css"%>
</style>

<script>
	function validate_item() {
		var el = document.forms["order_menu"]["scanCode"].value;
		return (el != null && el != "");
	}

	function validate_order() {
		var rows = document.getElementById("list").rows.length;
		return (rows != null && rows > 0);
	}
</script>

<%
	SimpleDateFormat sdf = new SimpleDateFormat("EEEEE, dd MMMM yyyy", Locale.FRANCE);
	String date = sdf.format(new Date());
%>
</head>
<body>
	<span class="badge badge-pill badge-info"><%=date%></span>
	<h1 align="center">Gestion de Stock</h1>
	<div class="container jumbotron">
		<form name="order_menu" action="GestionDeStock"
			onsubmit="return validate_item();" method="post">
			<h3>Commande: ${order.code}</h3>
			<table id="list" class="table">
				<tr>
					<th scope="col">Produit</th>
					<th scope="col">Prix</th>
				</tr>
				<c:forEach var="item" items="${order.items}">
					<tr>
						<td><c:out value="${item.name}" /></td>
						<td><c:out value="${item.price}" /></td>
					</tr>
				</c:forEach>
				<tr class="total">
					<th>Total</th>
					<td><fmt:formatNumber type="number" maxFractionDigits="2"
							value="${order.total}" /></td>
				</tr>
			</table>
			<div class="row">
				<div class="col-sm">
					<input class="form-control form-control-lg" type="text" name="scanCode">
				</div>
				<div class="col-sm">
					<button class="btn btn-primary btn-lg btn-block" type="submit" name="scanButton">Scanner</button>
				</div>
			</div>
		</form>
		<br>
		<form name="finish" action="GestionDeStock"
			onsubmit="return validate_order();" method="post">
			<button class="btn btn-success btn-lg btn-block" type="submit"
				name="orderButton">Payer</button>
		</form>
		<c:if test="${not empty message}">
			<div class="message">${message}</div>
		</c:if>
	</div>

	<a href="RegisterClosing">Etablissement de solde</a>
</body>
</html>