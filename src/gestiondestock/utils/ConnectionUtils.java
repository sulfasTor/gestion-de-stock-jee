package gestiondestock.utils;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
 
public class ConnectionUtils {
	
	private static Connection conn = null;
 
    public static Connection getConnection() 
              throws ClassNotFoundException, SQLException {
    	if (conn != null) {
			return conn;
		} else {
			Properties properties = readPropertiesFile();
			if (properties == null) {
				return conn;
			}
			String dbType = properties.getProperty("dbType", "mariadb");
			String hostName = properties.getProperty("hostName", "localhost");
			String dbName = properties.getProperty("dbName");
			String userName = properties.getProperty("userName", "root");
			String password = properties.getProperty("password");
			
			if (dbType.equals("mariadb"))
				conn = MariaDBConnUtils.getMariaDBConnection(hostName, dbName, userName, password);
			else if (dbType.equals("postgresql"))
				conn = PostgresqlConnUtils.getPostgresqlDBConnection(hostName, dbName, userName, password);
		}
        return  conn;
    }
     
    public static void closeQuietly(Connection conn) {
        try {
            conn.close();
        } catch (Exception e) {
        }
    }
 
    public static void rollbackQuietly(Connection conn) {
        try {
            conn.rollback();
        } catch (Exception e) {
        }
    }
    
	private static Properties readPropertiesFile() {
		Properties properties = new Properties();
		try {
			InputStream istream = ConnectionUtils.class.getClassLoader()
					.getResourceAsStream("db.properties");				
			if (properties != null) 
				properties.load(istream);
			
		} catch(Exception e) {
			e.printStackTrace();
		}	
		return properties;
	}
}